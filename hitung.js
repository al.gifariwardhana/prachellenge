function tambah(a, b) {
    let result = 0;
    let x = a + b
        // .. tulis jawaban kamu disini
    result += parseFloat(x);

    return result;
}

function kurang(a, b) {
    let result = 0;
    let x = a - b
        // .. tulis jawaban kamu disini
    result += parseFloat(x);
    return result;
}

function bagi(a, b) {
    let result = 0;
    let x = a / b
        // .. tulis jawaban kamu disini
    result += parseFloat(x);

    return result;
}

function kali(a, b) {
    let result = 0;
    let x = a * b
        // .. tulis jawaban kamu disini
    result += parseFloat(x);

    return result;
}

if (typeof window == 'undefined') {
    module.exports = { tambah, kurang, bagi, kali }
}